package programmer.zaman.now.data;
// absract di gunakan untuk melakukan kontrak dengan class child nya.
// abstact method hanya boleh mendefinisikan method nya saja.
public abstract class Animal {
    public String name;
    public abstract void run();
}
