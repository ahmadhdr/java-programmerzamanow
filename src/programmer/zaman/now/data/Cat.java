package programmer.zaman.now.data;

public class Cat extends Animal {
    // semua class yang mengextends ke dalam abstac class harus mendefinisikan semua method
    // di abstract class tersebut
    public void run() {
        System.out.println("Cat " + name + " is run");
    }
}
