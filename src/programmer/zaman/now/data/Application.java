package programmer.zaman.now.data;

public class Application {
    public static final int PROCESSOR;

    // di load pertama kali ketika class pertama kali di load, dan hanya bisa mengakses static property saja
    static {
        System.out.println("Mengakses class Application");
        PROCESSOR = Runtime.getRuntime().availableProcessors();
        System.out.println("AVAILIBLE PROCESSOR" + PROCESSOR);
    }
}
