package programmer.zaman.now.data;

// di interface kita bisa melakukan inheritance

import programmer.zaman.now.annotation.Fancy;

@Fancy(name = "Animal App", tags = {"application", "java"})
public interface Car extends HasBrand,IsMaintenance{
    // interface tidak bisa memiliki property tapi interface bisa memiliki constant
    // secara default method yang didefinisikan sudah menjadi absract ketika menggunakan interface
    void drive();
    int getTier();
    // default method di interface dibuat untuk mengsolve problem ketika sudah banyak class yang mengimplement
    // interface tersebut, bayangkan jika terdapat 100 class yang sudah mengimplement interface tersebut dan kita
    // menambahkan 1 method baru di interface tersebut, maka 100 class yang sudah mengimplement interface tersebut
    // akan menjadi rusak, dengan itulah default method di ciptkan.s
    default boolean isBig() {
        return false;
    }
}
