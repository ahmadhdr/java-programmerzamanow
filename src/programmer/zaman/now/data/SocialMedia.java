package programmer.zaman.now.data;

public class SocialMedia {
    String name;
}

//final class Facebook extends  SocialMedia {
//
//}

class Facebook extends  SocialMedia {
    void login(String username, String password) {
//        final void login(String username, String password) {

    }
}

// ketika class sudah menjadi final, maka kelas tersebut tidak bisa di extends kembali.
// ketika method sudah menjadi final, maka kelas turunan nya tidak bisa mengoveride method tersebut

class FakeFacebook extends Facebook {
    void login(String username, String password) {

    }
}