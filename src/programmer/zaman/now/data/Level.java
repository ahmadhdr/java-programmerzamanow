package programmer.zaman.now.data;

public enum Level {
//    STANDARD,
//    PREMIUM,
//    VIP
//
//
    // custructor enum
    STANDART("Standar level"), // call constructor
    PREMIUM("Premium Level"),
    VIP("Vip Level");

    private String description;

    Level(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }
}
