package programmer.zaman.now.data;

// untuk memakai lebih dari satu interface bisa menggunakan tanda koma
//    public class Avanza implements Car, IsMaintenance{

public class Avanza implements Car{

    // semua class yang mengimplement interface harus mendefinisikan semua method di dalam interfacenya
    public void drive() {
        System.out.println("Avanza drive");
    }

    public int getTier() {
        return 4;
    }

    public String getBrand() {
        return "Avanza";
    }

    public boolean isMaintenance() {
        return true;
    }
}
