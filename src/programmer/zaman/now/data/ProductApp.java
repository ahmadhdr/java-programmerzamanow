package programmer.zaman.now.data;

public class ProductApp {
    public static void main(String[] args) {
        Product product = new Product("Macbook Pro", 30000000);
        System.out.printf(product.getName());
        System.out.println(product.getPrice());

        // tujuan mengoveride toString agar classnya lebih mudah dibaca, karena by default method toClass
        // didalam class Object menggenerate seprti ini className + "@" +hashCode
        System.out.println(product);
    }
}
