package programmer.zaman.now.error;

public class BlankExeception extends RuntimeException{
    public BlankExeception(String message) {
        super(message);
    }
}
