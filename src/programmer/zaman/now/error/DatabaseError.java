package programmer.zaman.now.error;

// pada saat return error tidak di rekomendasikan di try catch, lebih baik dimatikan saja aplikasinya
public class DatabaseError extends Error{
    public DatabaseError(String message) {
        super(message);
    }
}
