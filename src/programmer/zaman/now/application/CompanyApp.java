package programmer.zaman.now.application;

import programmer.zaman.now.data.Company;

public class CompanyApp {
    public static void main(String[] args) {
        Company company = new Company();
        company.setName("Programmer zaman now");

        Company.Employe employe = company.new Employe();
        employe.setName("Eko");

        System.out.println(company.getName());
        System.out.println(employe.getName());
    }
}
