package programmer.zaman.now.application;

import programmer.zaman.now.data.Customer;
import programmer.zaman.now.data.Level;

public class EnumApp {
    public static void main(String[] args) {
        Customer customer = new Customer();
        customer.setName("Ahmad");
        customer.setLevel(Level.STANDART);

        System.out.println("customer name is :" + customer.getName());
        System.out.println("level name is :" + customer.getLevel());
        System.out.println("level description :" + customer.getLevel().getDescription());


        String levelName = Level.VIP.name();
        System.out.println(levelName);

        Level levelValueOf = Level.valueOf("VIP");
        System.out.println(levelValueOf);
    }
}
