package programmer.zaman.now.application;

import programmer.zaman.now.data.HelloWorld;

public class HelloWorldApp {
    public static void main(String[] args) {
        // anynoimouse class
        // membuat class langsung secara langsung tanpa menginstantiate
        // kekurangannya tidak bisa di reuse
        HelloWorld helloWorld = new HelloWorld() {
            public void sayHello() {

            }
            public void sayHello(String name) {

            }
        };
    }

    public static void run () {
        System.out.println("Run jalan");
    }
}