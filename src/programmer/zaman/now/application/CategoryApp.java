package programmer.zaman.now.application;

import programmer.zaman.now.data.Category;

public class CategoryApp {
    public static void main(String[] args) {
        Category category = new Category();
        category.setId("1123");
        category.setExpensive(true);

        System.out.println("the id is " + category.getId());
        System.out.println("the expensive is " + category.isExpensive());
    }
}
