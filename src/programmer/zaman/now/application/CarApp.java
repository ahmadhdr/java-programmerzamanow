package programmer.zaman.now.application;

import programmer.zaman.now.data.*;

public class CarApp {
    public static void main(String[] args) {
        // interface tidak bisa di instance tapi bisa di gunakan sebagai polymorph
        Car car = new Avanza();
        car.getTier();
        car.drive();
        System.out.println(car.getBrand());
    }
}
