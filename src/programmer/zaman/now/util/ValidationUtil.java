package programmer.zaman.now.util;

import programmer.zaman.now.annotation.NotBlank;
import programmer.zaman.now.error.BlankExeception;
import programmer.zaman.now.error.ValidationException;

import java.lang.reflect.Field;

public class ValidationUtil {
    public static void validationReflection(Object object) {
        Class aClass = object.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for(var field: fields) {
            field.setAccessible(true); // membuat property private bisa diakses
            if(field.getAnnotation(NotBlank.class) != null) {
                try {
                    String value = (String) field.get(object);
                    if(value == null || value.isBlank()) {
                        throw new BlankExeception("Field " + field.getName() + " is blank");
                    }
                }catch (IllegalAccessException exception) {

                    System.out.println("Tidak bisa mengakases : " + field.getName());
                }
            }
        }
    }
}

