class Employe {
    String name;
    // cara membuat constructor di java dengan cara membuat kembali nama class nya
    Employe(String name) {
        this.name = name;
    }

    void sayHello(String name) {
        System.out.println("Hello " + name + ", My name is employee" + this.name);
    }
}
