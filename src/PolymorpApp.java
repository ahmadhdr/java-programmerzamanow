class PolymorpApp {
    public static void main(String[] args) {
//        Employe employe = new Employe("Eko");
//        employe.sayHello("Budi");
//
//        employe = new Manager("Eko");
//        employe.sayHello("Budi");
//
//        employe = new VicePresident("Ekok");
//        employe.sayHello("Budi");
        sayHello(new Employe("ahmad"));
        sayHello(new Manager("haidar"));
        sayHello(new VicePresident("albaqir"));
    }


    // bayangkan jika tanpa employee kita harus membuat semua method yang sama disetiap anakannya.
    static void sayHello(Employe employe) {
        // type check agar aman

        if(employe instanceof  VicePresident) {
            // casting
            VicePresident vicePresident = (VicePresident) employe;
            System.out.println("Hello VP"+ vicePresident.name);
        }else if(employe instanceof  Manager) {
            Manager manager = (Manager) employe;
            System.out.println("Hello Manager" + manager.name);
        }else {
            System.out.println("Hello" + employe.name);
        }
    }
}
