class ManagerApp {
    public static void main(String[] args) {
        Manager manager = new Manager("haidar");
        manager.sayHello("Budi");

        VicePresident vicePresident = new VicePresident("aditya subagya");
        vicePresident.sayHello("Budi");
    }
}
